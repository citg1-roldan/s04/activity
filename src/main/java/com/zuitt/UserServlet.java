package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	
	public void init() throws ServletException {
		
		System.out.println("************************************");
		System.out.println("UserServlet has been initialized");
		System.out.println("************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res ) throws IOException {
		String fname= req.getParameter("fname");
		String lname= req.getParameter("lname");
		String email= req.getParameter("email");
		String contact= req.getParameter("contact");
		
		//first name - system properties
				System.setProperty("fname", fname);
				
		//last name - httpsession
				HttpSession session=req.getSession();
				session.setAttribute("lname", lname );
				
		//email - context params
				ServletContext srvContext=getServletContext();
				srvContext.setAttribute("email",email );
				
		//contact number - url rewriting via sendRedirect method
				res.sendRedirect("details?contact="+contact);


	}
	
	public void destroy(){
		
		System.out.println("************************************");
		System.out.println("UserServlet has been destroyed");
		System.out.println("************************************");
	}

}

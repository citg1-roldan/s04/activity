package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;

	public void init() throws ServletException {
		
		System.out.println("************************************");
		System.out.println("DetailsServlet has been initialized");
		System.out.println("************************************");
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		ServletContext srvContext = getServletContext();
		PrintWriter out=res.getWriter();
		
		//branding - servlet context params
			String branding =(String) srvContext.getInitParameter("branding");
		
		//first name - system properties
			String fname=System.getProperty("fname");
		
		//last name - HttpSession
			HttpSession session=req.getSession();		
			String lname= session.getAttribute("lname").toString();
		
		//email - servlet context		
			String email=(String) srvContext.getAttribute("email");
		
		//contact number - url rewriting via sendRedirect
			String contact= req.getParameter("contact"); 
		
		out.println("<h1>Welcome To Phone Book</h1>"
		+"<p>First Name:"+fname+"</p>"
		+"<p>Last Name:"+lname+"</p>"
		+"<p>Email:"+email+"</p>"
		+"<p>Contact:"+contact+"</p>"
		+"<p>Branding:"+branding+"</p>");
		
	}


	public void destroy(){
	
		System.out.println("************************************");
		System.out.println("DetailsServlet has been destroyed");
		System.out.println("************************************");
	}

}
